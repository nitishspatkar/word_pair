How to run the app
===================

1. Clone the repo
2. Navigate to the folder and do npm install (you must have node.js and npm installed)
3. To run the app : ionic serve

Logic:
======

1. It's a memory game where user has to memorize words  
2. The game has 6 levels

Level 1:
------------
Screen should be divided into two halves, left half contains a list of 15 words, and right half contains a list of 15 corresponding pair-words 
defined for the words in left side.

Initially the screen is blank only with a 'Start' button. When User clicks 'Start', level 1 starts (in each level clicking 'Start' will have different effects).
Both the lists are shown to the user for 35 seconds. Once 35 seconds are over: from left hand side list stays 3 words on the screen, 
right hand side list is replaced by input boxes and user is supposed to fill in correct pair for the word on the left side in the next 25 seconds.

If he succeeds, level is upgraded, if not he plays the same level again untill he finishes.

Level 2:
---------
Level 2 is similar to level 1, only this time after initial 35 seconds are over, input boxes will be on right hand side and pairs 
will be shown on the right hand side. User will have to fill in corresponding words for pairs on the right.

On success: level is upgraded, else same level untill success.

Level 3:
---------
Level 3 is similar to level 1, only this time initial 35 seconds are reduced to 30 seconds and once they are over, instead of 3 words 
this time 4 words will be presented. 

On success: level is upgraded, else same level untill success.

Level 4:
---------
Level 4 is similar to level 3 and 2, initial seconds 30 and 4 words, however input boxes will be on right like level 2.

On success: level is upgraded, else same level untill success.

Level 5:
--------
This level is totally different. This level includes drag-drop of 7 pairs out of 15 in right columns and right places.

Level 6:
--------
This level is totally different. This level includes 10 pairs to type freely.

Level 7:
--------
This level is similar to level 6. It includes all the 15 pairs to type freely.
