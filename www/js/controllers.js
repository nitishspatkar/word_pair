angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $timeout) {

  $scope.level=1
  $scope.leftWordList=false
  $scope.previewViewRight=false
  $scope.previewViewLeft=false
  $scope.enterTextViewRight=false
  $scope.enterTextViewLeft=false
  $scope.promptAction=''
  $scope.promptLevel=''
  $scope.promptFailure=''
  $scope.promptCongratulations=''
  $scope.enteredSequence= [];
  $scope.expectedPairSequece=[]
  $scope.expectedWordSequece=[]
  $scope.show_stop_button=false
  $scope.show_start_button=true

  $scope.counter1=0


  $scope.word_pair = [

  {'word':'Nitish', 'pair':'Patkar'},
  {'word':'Mihir', 'pair':'Janaj'},
  {'word':'Jannes', 'pair':'Stubbi'},
  {'word':'Martin', 'pair':'Wolle'}

  ]

  $scope.rightPartnerCheckList = {};
  for(var v in $scope.word_pair){

   $scope.expectedPairSequece.push($scope.word_pair[v].pair)
   $scope.rightPartnerCheckList[$scope.word_pair[v].word] = $scope.word_pair[v].pair;
 }

 $scope.leftPartnerCheckList = {};
 for(var v in $scope.word_pair){

   $scope.expectedWordSequece.push($scope.word_pair[v].word)
   $scope.leftPartnerCheckList[$scope.word_pair[v].pair] = $scope.word_pair[v].word;
 }
 
 $scope.showPartner = {};
 $scope.rightPartnerCheck = function(p,i_p){

   if($scope.rightPartnerCheckList[i_p] == p){
     $scope.showPartner[p] = true;
     if($scope.enteredSequence.indexOf(p)==-1){
      $scope.enteredSequence.push(p)
    }

    console.log($scope.expectedPairSequece)
    console.log($scope.enteredSequence)
  }
}

$scope.leftPartnerCheck = function(w,i_w){

 if($scope.leftPartnerCheckList[i_w] == w){
   $scope.showPartner[w] = true;
   if($scope.enteredSequence.indexOf(w)==-1){
     $scope.enteredSequence.push(w) 
   }
   
   console.log($scope.expectedWordSequece)
   console.log($scope.enteredSequence)
 }
}

$scope.start = function(){

  $scope.show_start_button=false
  
  $scope.promptAction=''
  $scope.promptCongratulations=''
  $scope.promptFailure=''
  $scope.counter1=0
  $scope.counter2=0
  $scope.counter3=0
  $scope.promptLevel='Level: ' + $scope.level

  if($scope.level===1){

    $scope.leftWordList=true
    $scope.previewViewRight=true

    $scope.counter1=5
    $timeout($scope.startFilling, 5000)
    $scope.onTimeout = function(){

      $scope.counter1--;
      mytimeout = $timeout($scope.onTimeout,1000);

      if($scope.counter1==0){
        $timeout.cancel(mytimeout);
        $scope.counter=0
      }
    }
    var mytimeout = $timeout($scope.onTimeout,1000); 

  }

  if($scope.level===2){

    console.log("Level 2")
    
    $scope.leftWordList=true
    $scope.previewViewRight=true

    $scope.counter2=5
    $timeout($scope.startFilling, 5000)
    $scope.onTimeout = function(){

      $scope.counter2--;
      mytimeout = $timeout($scope.onTimeout,1000);

      if($scope.counter2==0){
        $timeout.cancel(mytimeout);
      }
    }
    var mytimeout = $timeout($scope.onTimeout,1000); 
  }
}


$scope.startFilling = function(){

  $scope.promptLevel='Level: ' + $scope.level
  $scope.promptAction='Now enter the corresponding pairs in the right column'
  
  
  if($scope.level===1){

    $scope.counter3=20
    $scope.enterTextViewRight=true
    $scope.previewViewRight=false

  }

  if($scope.level===2){

    $scope.counter3=15
    $scope.leftWordList=false
    $scope.enterTextViewLeft=true
    $scope.previewViewRight=true

  }
  
  $scope.show_start_button=false
  $scope.show_stop_button=true
  

  $scope.onTimeout = function(){

    $scope.counter3--;
    mytimeout = $timeout($scope.onTimeout,1000);

    /* Need to implement logic to handle for $scope.expectedWordSequece in level 2*/

    if(angular.equals($scope.enteredSequence, $scope.expectedPairSequece)){

      $timeout.cancel(mytimeout);
      $scope.enteredSequence=[]

      if($scope.level===1){

       $scope.enterTextViewRight=false
       $scope.previewViewLeft=false
       $scope.leftWordList=false

     }

     if($scope.level===2){

      $scope.enterTextViewLeft=false
      $scope.previewViewRight=false


    }

    $scope.level= $scope.level + 1
    console.log("New level: "+ $scope.level)

    $scope.counter3=0
    $scope.promptLevel=''
    $scope.promptCongratulations='Congratulations!'
    $scope.promptAction=''
    $scope.promptFailure=''

    $scope.show_stop_button=false
    $scope.show_start_button=true

  }

  else if (angular.equals($scope.enteredSequence, $scope.expectedWordSequece)){

    $timeout.cancel(mytimeout);
    $scope.enteredSequence=[]

    if($scope.level===1){

     $scope.enterTextViewRight=false
     $scope.previewViewLeft=false
     $scope.leftWordList=false

   }

   if($scope.level===2){

    $scope.enterTextViewLeft=false
    $scope.previewViewRight=false


  }

  $scope.level= $scope.level + 1
  console.log("New level: "+ $scope.level)

  $scope.counter3=0
  $scope.promptLevel=''
  $scope.promptCongratulations='Congratulations!'
  $scope.promptAction=''
  $scope.promptFailure=''

  $scope.show_stop_button=false
  $scope.show_start_button=true

}

else if($scope.counter3==0){

  console.log("Counter 0")

  $scope.promptCongratulations=''
  $timeout.cancel(mytimeout);
  $scope.enteredSequence=[]
  $scope.enterTextViewRight=false
  $scope.enterTextViewLeft=false
  $scope.previewViewLeft=false
  $scope.previewViewRight=false
  $scope.leftWordList=false
  $scope.show_stop_button=false
  $scope.promptAction=''
  $scope.counter3=0
  $scope.promptFailure='Failed! Try again.'
  $scope.level=$scope.level
  $scope.show_start_button=true


}
}
var mytimeout = $timeout($scope.onTimeout,1000);


}

})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
